# Move_Grid_Demo

Using Unity 2018.3.2f1

How to: 
-Pull from the develop branch
-Open the project in Untiy 3D
-Open Main scene
-Run the game

Additional notes
-To change the grid size navigate to Assets/GamePlay/Grid/Prefabs/Grid.pref and in the inspector change the grid rows and columns in the GridData component.
-To change player speed navigate to Assets/GamePlay/PlayerData/Config/PlayerData.asset and change the speed value in the scriptable object.
-Swipe lenght required for valid input can be changed in the SwipeData component attached to SwipeInput gameobject in the hierchy