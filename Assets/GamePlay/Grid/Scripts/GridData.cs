﻿using UnityEngine;

/// <summary>
/// Data for the grid size.
/// </summary>
public class GridData : MonoBehaviour
{
    public int rows = 0;
    public int columns = 0;
}
